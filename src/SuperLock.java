import java.util.Arrays;

public class SuperLock {
    public static void main(String[] args) {
        int[] password = new int[10];
        password[0] = 2;
        password[1] = 6;
        checkTheDoor(password);
    }

    public static void checkTheDoor(int[] password) {
        int sum = 10;
        for (int i = 1; i < password.length - 1; i++) {
            if (i == 1) {
                if (password[i-1] + password[i] + password[i+1] != sum){
                    password[i+1] = 10 - (password[i] + password[i-1]);
                }
            } else {
                if (password[i-1] + password[i] + password[i+1] != sum){
                    password[i+1] = 10 - (password[i] + password[i-1]);
                }
            }
        }
        System.out.println(Arrays.toString(password) + " door opened!");
    }
}

